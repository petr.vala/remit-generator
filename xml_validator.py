# This Python file uses the following encoding: utf-8
# XML validator
# version 0.1 [17.02.2016]

import os
import sys
import datetime
import argparse

from lxml import etree

import logging

logger = logging.getLogger(__name__)
handler = logging.StreamHandler(stream=sys.stdout)
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
formatter = logging.Formatter('%(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

__version__ = "0.1"
__date__ = "17.02.2016"


def validate(xml_file, schema_file):
    """Validate xml file against xsd schema

    :param xml_file: file name of the xml file
    :type xml_file: str
    :param schema_file: file name of the xsd schema
    :type schema_file: str
    """

    if not os.path.isfile(xml_file):
        logger.error("Cannot find xml file: %s", xml_file)
        return -1
    elif not os.path.isfile(schema_file):
        logger.error("Cannot find schema file: %s", schema_file)
        return -1
    else:
        logger.info("Validation of %s xml file against %s schema file", xml_file, schema_file)

        xsd_doc = etree.parse(schema_file)
        xsd = etree.XMLSchema(xsd_doc)

        xml = etree.parse(xml_file)
        result = xsd.validate(xml)

        logger.info("Validation result: %s", result)
        if not result:
            logger.error("Validation log:\n%s", xsd.error_log)

        return 0


if __name__ == "__main__":

    # start counting time and print out version info
    script_started = datetime.datetime.now()
    logger.info("version %s [%s]", __version__, __date__)

    # resolve arguments
    parser = argparse.ArgumentParser(description="Validate xml file against xsd schema.")
    parser.add_argument("xml_file", type=str, help="input file in xml format")
    parser.add_argument("xsd_file", type=str, help="input file in xsd format")
    args = parser.parse_args()

    logger.info("%s : script started", script_started)

    if os.path.isfile(args.xml_file):
        if os.path.isfile(args.xsd_file):
            ret_val = validate(xml_file=args.xml_file, schema_file=args.xsd_file)
        else:
            logger.error("Cannot find schema file: %s", args.xsd_file)
    else:
        logger.error("Cannot find xml file: %s", args.xml_file)

    # finish info
    script_finished = datetime.datetime.now()
    logger.info("%s : ALL DONE in %s", script_finished, script_finished - script_started)
