# REMIT XML report generation and validation tools
Set of tools to help fellow colleagues to fulfil REMIT reporting obligation.

Originaly developed as a basic backloading tool for Phase 1 of REMIT reporting.
I decided to enhance it for more complex functionality and to make it public.

Only ACER Table 1 supported for the time being.

>All file are free to use, copy, edit or distribute.

Be informed that all files are publicly available as they are and correct
functionality is not guaranteed.   
 
 
Info for developers:

Coded in _Python 2.7_ [https://www.python.org/](https://www.python.org/) with
help of these external libraries:
  - _lxml_ [http://lxml.de/](http://lxml.de/) for XML manipulation and
    validation
  - _pywin32_ [https://sourceforge.net/projects/pywin32/]
     (https://sourceforge.net/projects/pywin32/) for Excel manipulation


Developed by Petr Vala [p.vala@sev-en.cz](mailto:p.vala@sev-en.cz) for Sev.en EC
[http://www.7ec.cz](http://www.7ec.cz), member of Sev.en group
[http://www.sev-en.cz/en/spolecnost/](http://www.sev-en.cz/en/spolecnost/).

Feedback would be highly appreciated as I am thinking about creating similar
tool for ACER Table 2.


## remit_generator.py / .exe
--------------------------------------------------------------------------------
Main tool to generate xml report file from MS Excel sheet.

Best to run in Windows console (cmd.exe).

Try to run `remit_generator.exe -h` for the list of possible arguments.


Example of usage:

`remit_generator.exe template.xls -o myoutputfile.xml -v REMITTable1_V2.xsd`

Use _template.xls_ as the input (load all rows from _Reporting_ sheet), create
output file named _myoutputfile.xml_ and validate it against
_REMITTable1_V2.xsd_ schema file provided by ACER
[https://www.acer-remit.eu/portal/data-submission]
(https://www.acer-remit.eu/portal/data-submission).

Only _inputfile_ argument is mandatory.
 
 
**template.xlsx**

This is a basic template to serve as an input for the generation tool.

Only _Reporting_ sheet is mandatory. Do not rename it or change the arrangement
of columns if you do not want to recode the tool.

Beware of the correct fields' format. Some examples are included. 

Other sheets are just examples of output from a trading system and how to
convert them into formated fields in the _Reporting_ sheet.

There is a limitation of number of fields 53 and 54 to some most used products.
Price Interval Quantity Details are supported only for contracts using single
field 53 and 54 values (f.e. baseload product 00:00/00:00, weekdays and weekends
not supported). I have not decided yet how to omit/solve these restrictions as
we currently do not need it.


## xml_validator.py / .exe
--------------------------------------------------------------------------------
Simple tool to validate xml report file against xsd schema.

Best to run in Windows console (cmd.exe).

Try to run `xml_validator.exe -h` for the list of possible arguments.


Example of usage:

`xml_validator.exe reportfile.xml REMITTable1_V2.xsd`

Use _reportfile.xml_ as the input and validate it against
_REMITTable1_V2.xsd_ schema file provided by ACER
[https://www.acer-remit.eu/portal/data-submission]
(https://www.acer-remit.eu/portal/data-submission).

Both _xml_file_ and _xsd_file_ arguments are mandatory.


--------------------------------------------------------------------------------
### Possible future enhancements:
Omit using _pywin32_ and use _openpyxl_ instead. Thus no need of MS Excel and
to become a multiplatform tool.

Automatic detection of used range of columns and automatically detect fields'
numbers. More complex contract could be reported then.

More possible input formats (f.e. csv files).    