# This Python file uses the following encoding: utf-8
# REMIT Table 1 xml generator according to the ACER definitions
# version 0.2.3 [07.03.2016]

import os
import sys
import datetime
import argparse

from lxml import etree
import win32com.client as win32
from win32com.client import constants as c
import pywintypes

import logging

logger = logging.getLogger(__name__)
handler = logging.StreamHandler(stream=sys.stdout)
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
formatter = logging.Formatter('%(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

__version__ = "0.2.3"
__date__ = "07.03.2016"


def load_contracts_from_excel_file(file_name):
    """Load contracts from excel file.

    :param file_name: path and name of the file
    :type file_name: str
    """

    if os.path.isfile(file_name):
        logger.debug("Loading %s", file_name)
    else:
        logger.critical("Cannot find %s", file_name)
        sys.exit(-1)

    if win32.gencache.is_readonly:
        # allow gencache to create the cached wrapper objects
        win32.gencache.is_readonly = False

        # under p2exe the call in gencache to __init__() does not happen
        # so we use Rebuild() to force the creation of the gen_py folder
        win32.gencache.Rebuild()

        # NB You must ensure that the python...\win32com.client.gen_py dir does not exist
        # to allow creation of the cache in %temp%

    # test whether excel has been already loaded
    try:
        win32.GetActiveObject("Excel.Application")
        excel_running = True
    except pywintypes.com_error:
        excel_running = False
        logger.debug("Starting Excel application...")

    excel = win32.gencache.EnsureDispatch("Excel.Application")
    logger.debug("Excel started")
    wb = excel.Workbooks.Open(file_name)
    logger.debug("Excel file opened")
    if not excel_running:
        excel.Visible = False

    # select sheet with data
    sheet = wb.Sheets("Reporting")

    # select ranges - CONSTANTS USED - CHECK FOR CORRECTNESS! to be automatized
    top_left = [2, 1]  # [row, column]
    last_row = sheet.Range("A" + str(sheet.Rows.Count)).End(c.xlUp).Row
    bottom_right = [last_row, 47]
    logger.debug("Excel last row: %d", last_row)

    # load selected data
    contracts_data = sheet.Range(sheet.Cells(top_left[0], top_left[1]),
                                 sheet.Cells(bottom_right[0], bottom_right[1])).Value

    if not excel_running:
        wb.Close(False)
        logger.debug("Excel file closed")
        excel.Application.Quit()
        logger.debug("Excel closed")

    return contracts_data


def prepare_input_data(excel_file, limit=0):
    """Initialize data structures from excel file and return a list of REMIT tables 1 data

    :param excel_file: path and name to excel file with input data
    :type excel_file: str
    :param limit: how many deals/excel rows to load (0 = all)
    :type limit: int
    """

    excel_data = load_contracts_from_excel_file(excel_file)
    no_of_contracts = len(excel_data)
    logger.debug("Limit parameter: %d", limit)
    logger.debug("Length of excel data: %d", no_of_contracts)

    if limit <= 0:
        limit = no_of_contracts
    else:
        limit = min(limit, no_of_contracts)

    logger.info("Contracts found: %d | contracts to be loaded: %d", no_of_contracts, limit)

    contracts_data = []
    # loop all rows loaded from excel
    for i in range(0, limit):
        logger.debug("Reading deal #%d...", i+1)
        contracts_data.append(RemitTableData(excel_data[i]))

    logger.debug("Length of contracts_data: %d", len(contracts_data))

    return contracts_data


def validate(xml_file, schema_file):
    """Validate generated xml file against xsd schema

    :param xml_file: file name of the xml file
    :type xml_file: str
    :param schema_file: file name of the xsd schema
    :type schema_file: str
    """

    if not os.path.isfile(xml_file):
        logger.error("Cannot find %s xml file.", xml_file)
        return -1
    elif not os.path.isfile(schema_file):
        logger.error("Cannot find %s schema file.", schema_file)
        return -1
    else:
        logger.info("Validation of %s xml file against %s schema file", xml_file, schema_file)

        xsd_doc = etree.parse(schema_file)
        xsd = etree.XMLSchema(xsd_doc)

        xml = etree.parse(xml_file)
        result = xsd.validate(xml)

        logger.info("Validation result: %s", result)
        if not result:
            logger.error("Validation log:\n%s", xsd.error_log)

        return 0


class RemitTableData:
    """Structure of one table data"""

    def __init__(self, data_list):
        """Initialize the structure. To be automatized and to implement input data testing.

        :param data_list: input data, one row loaded from excel file
        :type data_list: list
        """

        # Parties to the contract
        self.val_1 = str(self.n2e(data_list[0]))
        self.val_2 = str(self.n2e(data_list[1]))
        if self.val_2 == "" and self.val_1 != "":
            self.val_2 = self.get_type_of_code(self.val_1)
        self.val_3 = str(self.n2e(data_list[2]))
        self.val_4 = str(self.n2e(data_list[3]))
        self.val_5 = str(self.n2e(data_list[4]))
        if self.val_5 == "" and self.val_4 != "":
            self.val_5 = self.get_type_of_code(self.val_4)
        self.val_6 = str(self.n2e(data_list[5]))
        self.val_7 = str(self.n2e(data_list[6]))
        if self.val_7 == "" and self.val_6 != "":
            self.val_7 = self.get_type_of_code(self.val_6)
        self.val_8 = str(self.n2e(data_list[7]))
        self.val_9 = str(self.n2e(data_list[8]))
        if self.val_9 == "" and self.val_8 != "":
            self.val_9 = self.get_type_of_code(self.val_8)
        self.val_10 = str(self.n2e(data_list[9]))
        self.val_11 = str(self.n2e(data_list[10]))
        # val_12 not implemented

        # Order details - not implemented

        # Contract details
        self.val_21 = str(self.n2e(data_list[11]))
        self.val_22 = str(self.n2e(data_list[12]))
        self.val_23 = str(self.n2e(data_list[13]))
        self.val_24 = str(self.n2e(data_list[14]))
        self.val_25 = str(self.n2e(data_list[15]))
        self.val_26 = str(self.n2e(data_list[16]))
        self.val_27 = str(self.n2e(data_list[17]))
        self.val_28 = str(self.n2e(data_list[18]))
        # val_29 not implemented

        # Transaction Details
        self.val_30 = str(self.n2e(data_list[19]))
        self.val_31 = str(self.n2e(data_list[20]))
        self.val_32 = str(self.n2e(data_list[21]))
        # val_33 not implemented
        # val_34 not implemented
        self.val_35 = str(self.n2e(data_list[22]))  # changed from float
        self.val_36 = str(self.n2e(data_list[23]))  # not used!
        if self.val_36 != "":
            logger.warning("Do not use Field 36: Index Value. Use Field 35: Price instead.")
        self.val_37 = str(self.n2e(data_list[24]))
        self.val_38 = str(self.n2e(data_list[25]))  # changed from float
        self.val_39 = str(self.n2e(data_list[26]))
        self.val_40 = str(self.n2e(data_list[27]))  # changed from float
        self.val_41 = str(self.n2e(data_list[28]))  # changed from float
        self.val_42 = str(self.n2e(data_list[29]))
        self.val_43 = str(self.n2e(data_list[30]))

        # Option details
        option_test = 0
        self.val_44 = str(self.n2e(data_list[31]))
        if self.val_44 != "":
            option_test += 1
        self.val_45 = str(self.n2e(data_list[32]))
        if self.val_45 != "":
            option_test += 1
        self.val_46 = str(self.n2e(data_list[33]))
        if self.val_46 != "":
            option_test += 1
        self.val_47 = str(self.n2e(data_list[34]))  # changed from float
        if self.val_47 != "":
            option_test += 1

        if option_test in range(1, 4):
            logger.warning("You have to use all four fields (44-47) to define an option.")

        # Delivery profile
        self.val_48 = str(self.n2e(data_list[35]))
        self.val_49 = str(self.n2e(data_list[36]))
        self.val_50 = str(self.n2e(data_list[37]))
        self.val_51 = str(self.n2e(data_list[38]))
        if self.val_51 != "":
            logger.info("Field 51: Duration is not required to be reported. (See ACER FAQ document.)")
        self.val_52 = str(self.n2e(data_list[39]))
        self.val_53 = [str(self.n2e(data_list[40])), str(self.n2e(data_list[43]))]
        self.val_54 = [[str(self.n2e(data_list[41])), str(self.n2e(data_list[42]))], str(self.n2e(data_list[44]))]
        # val_55 not implemented
        # val_56 not implemented
        # val_57 not implemented

        # Lifecycle information
        self.val_58 = str(self.n2e(data_list[45]))

        # Price Interval Quantity Details / Executions
        self.val_execution = str(self.n2e(data_list[46]))
        if self.val_execution == "YES" and self.val_32 == "":
            logger.warning("Deal marked as Execution, but no Linked Transaction ID present.")

    def __str__(self):
        """Print all data."""

        return "%s" % self.__dict__

    @staticmethod
    def get_type_of_code(code):
        """Get type of code i.e. ACE, LEI, BIC, EIC, GLN and return it in lowercase.
        :param code: alphanumerical code
        :type code: str
        """

        if len(code) == 12:
            return "ace"
        elif len(code) == 20:
            return "lei"
        elif len(code) == 11:
            return "bic"
        elif len(code) == 16:
            return "eic"
        elif len(code) == 13:
            return "gln"
        else:
            logger.warning("Cannot resolve type of code: %s! Returning err instead.", code)
            return "err"

    @staticmethod
    def n2e(value):
        """Return empty string if value equals None or return the same value. (None to empty)
        :param value: value
        :type value: any type
        """

        if value is None:
            return ""
        else:
            return value


class RemitTable1:
    """Create REMIT Table 1 report."""

    def __init__(self):
        """Initialize report with the top element REMITTable1."""

        my_namespaces = {'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                         None: 'http://www.acer.europa.eu/REMIT/REMITTable1_V2.xsd'}

        self.remit_table = etree.Element("REMITTable1", nsmap=my_namespaces)
        self.report = etree.ElementTree(self.remit_table)

    def __str__(self, declaration=False):
        """Print out readable report.

        :param declaration: add XML declaration
        :type declaration: bool
        """

        return "%s" % etree.tostring(self.report, pretty_print=True, encoding="utf-8", xml_declaration=declaration)

    def save(self, file_name="output.xml"):
        """Save report to file

        :param file_name: name of the output file
        :type file_name: str
        """

        try:
            with open(file_name, "w") as fn:
                self.report.write(fn, pretty_print=True, encoding="utf-8", xml_declaration=True)
                logger.info("Report saved as: %s ", file_name)
        except IOError:
            logger.error("Cannot write report to disk!")

    def check_reporting_entity_id(self, type_of_code, re_id):
        """Check whether reporting entity id already exists and compare it with data from parameters -> bool

        :param type_of_code: type of code of reporting entity id (ACE, LEI, BIC, EIC, GLN)
        :type type_of_code: str
        :param re_id: reporting entity id
        :type re_id: str
        """

        text = self.remit_table.find(".//reportingEntityID")
        if text is not None and len(text) > 0:

            # test equality of values
            if text[0].tag != type_of_code:
                logger.warning("Reporting entity type of code differ! Using the former one.")
            if text[0].text != re_id:
                logger.warning("Reporting entity ID differ! Using the former one.")

            return True
        else:
            return False

    def add_data(self, list_of_contracts_data):
        """Take list of data and create particular structure.

        :param list_of_contracts_data: list of input data to be added to the Table 1 in defined structure
        :type list_of_contracts_data: list of RemitTableData
        """

        if len(list_of_contracts_data) == 0:
            logger.warning("Empty list of contracts data.")
            return 1

        reporting_entity_id = etree.SubElement(self.remit_table, "reportingEntityID")
        trade_list = etree.SubElement(self.remit_table, "TradeList")

        for counter, one_contract in enumerate(list_of_contracts_data):
            logger.debug("Creating structure for contract #%d", (counter+1))

            # reporting entity is defined only once for the whole set - must be the same for the whole data set!
            if not self.check_reporting_entity_id(type_of_code=one_contract.val_7.lower(), re_id=one_contract.val_6):
                # #7
                type_of_code = etree.SubElement(reporting_entity_id, one_contract.val_7.lower())
                # #6
                type_of_code.text = one_contract.val_6

            trade_report = etree.SubElement(trade_list, "TradeReport")

            record_seq_number = etree.SubElement(trade_report, "RecordSeqNumber")
            record_seq_number.text = str(counter + 1)

            id_of_market_participant = etree.SubElement(trade_report, "idOfMarketParticipant")
            # #2
            id_of_market_participant_idtype = etree.SubElement(id_of_market_participant, one_contract.val_2.lower())
            # #1
            id_of_market_participant_idtype.text = one_contract.val_1

            if one_contract.val_3 != "":
                trader_id = etree.SubElement(trade_report, "traderID")
                # #3
                trader_if_fmp = etree.SubElement(trader_id, "traderIdForMarketParticipant")
                trader_if_fmp.text = one_contract.val_3

            if one_contract.val_4 != "":
                other_market_participant = etree.SubElement(trade_report, "otherMarketParticipant")
                # #5
                other_market_participant_idtype = etree.SubElement(other_market_participant, one_contract.val_5.lower())
                # #4
                other_market_participant_idtype.text = one_contract.val_4

            if one_contract.val_8 != "":
                beneficiary_identification = etree.SubElement(trade_report, "beneficiaryIdentification")
                # #9
                beneficiary_identification_idtype = etree.SubElement(beneficiary_identification,
                                                                     one_contract.val_9.lower())
                # #8
                beneficiary_identification_idtype.text = one_contract.val_8

            trading_capacity = etree.SubElement(trade_report, "tradingCapacity")
            # #10
            trading_capacity.text = one_contract.val_10

            buy_sell_indicator = etree.SubElement(trade_report, "buySellIndicator")
            # #11
            buy_sell_indicator.text = one_contract.val_11

            # contract info - begin
            contract_info = etree.SubElement(trade_report, "contractInfo")

            contract = etree.SubElement(contract_info, "contract")

            contract_id = etree.SubElement(contract, "contractId")
            # #21
            contract_id.text = one_contract.val_21

            contract_name = etree.SubElement(contract, "contractName")
            # #22
            contract_name.text = one_contract.val_22

            contract_type = etree.SubElement(contract, "contractType")
            # #23
            contract_type.text = one_contract.val_23

            energy_commodity = etree.SubElement(contract, "energyCommodity")
            # #24
            energy_commodity.text = one_contract.val_24

            if one_contract.val_25 != "":
                fixing_index = etree.SubElement(contract, "fixingIndex")
                index_name = etree.SubElement(fixing_index, "indexName")
                # #25
                index_name.text = one_contract.val_25

            settlement_method = etree.SubElement(contract, "settlementMethod")
            # #26
            settlement_method.text = one_contract.val_26

            organised_market_place_identifier = etree.SubElement(contract, "organisedMarketPlaceIdentifier")
            # #27
            if len(one_contract.val_27) == 4:
                if one_contract.val_27.upper() == "XBIL":
                    ompi_type = etree.SubElement(organised_market_place_identifier, "bil")
                else:
                    ompi_type = etree.SubElement(organised_market_place_identifier, "mic")
            elif len(one_contract.val_27) == 12:
                ompi_type = etree.SubElement(organised_market_place_identifier, "ace")
            elif len(one_contract.val_27) == 20:
                ompi_type = etree.SubElement(organised_market_place_identifier, "lei")
            else:
                logger.error("Cannot resolve organisedMarketPlaceIdentifier at contract #%d - using <error> tag",
                             counter+1)
                ompi_type = etree.SubElement(organised_market_place_identifier, "error")
            ompi_type.text = one_contract.val_27

            contract_trading_hours = etree.SubElement(contract, "contractTradingHours")
            # #28
            cth_values = one_contract.val_28.split("/")
            cth_start_time = etree.SubElement(contract_trading_hours, "startTime")
            cth_start_time.text = cth_values[0]
            cth_end_time = etree.SubElement(contract_trading_hours, "endTime")
            cth_end_time.text = cth_values[1]

            # options
            if one_contract.val_44 != "" \
                    and one_contract.val_45 != "" \
                    and one_contract.val_46 != "" \
                    and one_contract.val_47 != "":

                option_details = etree.SubElement(contract, "optionDetails")

                option_style = etree.SubElement(option_details, "optionStyle")
                # #44
                option_style.text = one_contract.val_44

                option_type = etree.SubElement(option_details, "optionType")
                # #45
                option_type.text = one_contract.val_45

                option_exercise_date = etree.SubElement(option_details, "optionExerciseDate")
                # #46
                option_exercise_date.text = one_contract.val_46

                option_strike_price = etree.SubElement(option_details, "optionStrikePrice")

                osp_value = etree.SubElement(option_strike_price, "value")
                # #47
                osp_value.text = one_contract.val_47

                osp_currency = etree.SubElement(option_strike_price, "currency")
                # #37
                osp_currency.text = one_contract.val_37

            delivery_point_or_zone = etree.SubElement(contract, "deliveryPointOrZone")
            # #48
            delivery_point_or_zone.text = one_contract.val_48

            delivery_start_date = etree.SubElement(contract, "deliveryStartDate")
            # #49
            delivery_start_date.text = one_contract.val_49

            delivery_end_date = etree.SubElement(contract, "deliveryEndDate")
            # #50
            delivery_end_date.text = one_contract.val_50

            if one_contract.val_51 != "":
                duration = etree.SubElement(contract, "duration")
                # #51
                duration.text = one_contract.val_51

            load_type = etree.SubElement(contract, "loadType")
            # #52
            load_type.text = one_contract.val_52

            delivery_profile = etree.SubElement(contract, "deliveryProfile")
            # #53 @1
            if one_contract.val_53[0] != "":
                days_of_the_week_1 = etree.SubElement(delivery_profile, "daysOfTheWeek")
                days_of_the_week_1.text = one_contract.val_53[0]

            # 54 @1.1
            periods = one_contract.val_54[0][0].split("/")
            load_delivery_start_time_1_1 = etree.SubElement(delivery_profile, "loadDeliveryStartTime")
            load_delivery_start_time_1_1.text = periods[0] + ":00"
            load_delivery_end_time_1_1 = etree.SubElement(delivery_profile, "loadDeliveryEndTime")
            load_delivery_end_time_1_1.text = periods[1] + ":00"

            # #54 @1.2
            if one_contract.val_54[0][1] != "":
                periods = one_contract.val_54[0][1].split("/")
                load_delivery_start_time_1_2 = etree.SubElement(delivery_profile, "loadDeliveryStartTime")
                load_delivery_start_time_1_2.text = periods[0] + ":00"
                load_delivery_end_time_1_2 = etree.SubElement(delivery_profile, "loadDeliveryEndTime")
                load_delivery_end_time_1_2.text = periods[1] + ":00"

            # #53 @2 #54 @2
            if one_contract.val_53[1] != "":
                delivery_profile_2 = etree.SubElement(contract, "deliveryProfile")
                days_of_the_week_2 = etree.SubElement(delivery_profile_2, "daysOfTheWeek")
                days_of_the_week_2.text = one_contract.val_53[1]

                periods = one_contract.val_54[1].split("/")
                load_delivery_start_time_2 = etree.SubElement(delivery_profile_2, "loadDeliveryStartTime")
                load_delivery_start_time_2.text = periods[0] + ":00"
                load_delivery_end_time_2 = etree.SubElement(delivery_profile_2, "loadDeliveryEndTime")
                load_delivery_end_time_2.text = periods[1] + ":00"

            # contract info - end

            organised_market_place_identifier_2 = etree.SubElement(trade_report, "organisedMarketPlaceIdentifier")
            # #27
            if len(one_contract.val_27) == 4:
                if one_contract.val_27.upper() == "XBIL":
                    ompi_type_2 = etree.SubElement(organised_market_place_identifier_2, "bil")
                else:
                    ompi_type_2 = etree.SubElement(organised_market_place_identifier_2, "mic")
            elif len(one_contract.val_27) == 12:
                ompi_type_2 = etree.SubElement(organised_market_place_identifier_2, "ace")
            elif len(one_contract.val_27) == 20:
                ompi_type_2 = etree.SubElement(organised_market_place_identifier_2, "lei")
            else:
                logger.error("Cannot resolve organisedMarketPlaceIdentifier at contract #%d - using <error> tag",
                             counter+1)
                ompi_type_2 = etree.SubElement(organised_market_place_identifier_2, "error")
            ompi_type_2.text = one_contract.val_27

            transaction_time = etree.SubElement(trade_report, "transactionTime")
            # #30
            transaction_time.text = one_contract.val_30

            unique_transaction_identifier_1 = etree.SubElement(trade_report, "uniqueTransactionIdentifier")
            unique_transaction_identifier_2 = etree.SubElement(unique_transaction_identifier_1,
                                                               "uniqueTransactionIdentifier")
            # #31
            unique_transaction_identifier_2.text = one_contract.val_31

            # #32
            if one_contract.val_32 != "":
                linked_transaction_id = etree.SubElement(trade_report, "linkedTransactionId")
                linked_transaction_id.text = one_contract.val_32

            if one_contract.val_35 != "":
                price_details = etree.SubElement(trade_report, "priceDetails")
                price = etree.SubElement(price_details, "price")
                # #35
                price.text = one_contract.val_35

                price_currency = etree.SubElement(price_details, "priceCurrency")
                # #37
                price_currency.text = one_contract.val_37

            notional_amount_details = etree.SubElement(trade_report, "notionalAmountDetails")
            notional_amount = etree.SubElement(notional_amount_details, "notionalAmount")
            # #38
            notional_amount.text = one_contract.val_38

            notional_currency = etree.SubElement(notional_amount_details, "notionalCurrency")
            # #39
            notional_currency.text = one_contract.val_39

            if one_contract.val_40 != "":
                quantity = etree.SubElement(trade_report, "quantity")
                value_1 = etree.SubElement(quantity, "value")
                # #40
                value_1.text = one_contract.val_40

                unit_1 = etree.SubElement(quantity, "unit")
                # #42 /1
                unit_1.text = one_contract.val_42.split("/")[0]

            total_notional_contract_quantity = etree.SubElement(trade_report, "totalNotionalContractQuantity")
            value_2 = etree.SubElement(total_notional_contract_quantity, "value")
            # #41
            value_2.text = one_contract.val_41

            unit_2 = etree.SubElement(total_notional_contract_quantity, "unit")
            # #42 /2
            unit_2.text = one_contract.val_42.split("/")[1]

            # Price Interval Quantity Details
            if one_contract.val_execution == "YES":

                if one_contract.val_53[1] != "" or one_contract.val_54[0][1] != "":
                    logger.warning("Only executions of simple products are supported so far.")
                    logger.error("Cannot create Price Interval Quantity Details elements.")
                else:

                    piqd = etree.SubElement(trade_report, "priceIntervalQuantityDetails")

                    interval_start_date = etree.SubElement(piqd, "intervalStartDate")
                    # 49
                    interval_start_date.text = one_contract.val_49

                    interval_end_date = etree.SubElement(piqd, "intervalEndDate")
                    # 50
                    interval_end_date.text = one_contract.val_50

                    # 54 @1.1
                    periods = one_contract.val_54[0][0].split("/")
                    interval_start_time = etree.SubElement(piqd, "intervalStartTime")
                    interval_start_time.text = periods[0] + ":00"
                    interval_end_time = etree.SubElement(piqd, "intervalEndTime")
                    interval_end_time.text = periods[1] + ":00"

            if one_contract.val_43 != "":
                termination_date = etree.SubElement(trade_report, "terminationDate")
                # #43
                termination_date.text = one_contract.val_43

            action_type = etree.SubElement(trade_report, "actionType")
            # #58
            action_type.text = one_contract.val_58


if __name__ == "__main__":

    # start counting time and print out version info
    script_started = datetime.datetime.now()
    logger.info("version %s [%s]", __version__, __date__)

    # resolve possible arguments
    parser = argparse.ArgumentParser(description="Create REMIT XML report.")
    parser.add_argument("inputfile", type=str, help="input filename in MS Excel format")
    parser.add_argument("-f", "--first", metavar="integer", type=int,
                        help="process only first contracts up to the limit")
    parser.add_argument("-v", "--validate", metavar="filename", type=str,
                        help="validate resulting xml file with validation schema as filename")
    parser.add_argument("-l", "--loglevel", choices=["debug", "info", "warning", "error"], type=str,
                        help="change log level from default [info]")
    parser.add_argument("-o", "--output", metavar="filename", type=str,
                        help="output filename")
    args = parser.parse_args()

    allowed_log_levels = {"debug": logging.DEBUG, "info": logging.INFO,
                          "warning": logging.WARNING, "error": logging.ERROR}
    if args.loglevel is not None:
        if args.loglevel in allowed_log_levels:
            logger.setLevel(allowed_log_levels[args.loglevel])
            logger.info("Logging level changed to: %s", args.loglevel)

    # start info
    logger.info("%s : script started", script_started)

    if args.first is not None:
        limit_contracts = args.first
        logger.debug("Contracts limited by argument: %d", limit_contracts)
    else:
        limit_contracts = 0

    if os.path.isfile(args.inputfile):
        try:
            path = os.path.dirname(os.path.abspath(__file__))
        except NameError:
            path = os.path.dirname(os.path.abspath(sys.argv[0]))  # needed for py2exe compiler
        input_file = path + "\\" + args.inputfile
    else:
        logger.critical("Cannot find input file.")
        sys.exit(-1)

    if args.output is not None:
        xml_filename = args.output
        logger.debug("Output file named by argument: %s", xml_filename)
    else:
        xml_filename = "output.xml"
        logger.debug("Using default output file name: %s", xml_filename)

    schema_filename = ""
    if args.validate is not None:
        if os.path.isfile(args.validate):
            schema_filename = args.validate
            logger.debug("Validation switched on. Schema file: %s", schema_filename)
        else:
            logger.error("Validation switched off. Cannot find schema file: %s", args.validate)
    else:
        logger.debug("Validation switched off.")

    # load input data from excel file to list of RemitTableData structures
    remit_data = prepare_input_data(excel_file=input_file, limit=limit_contracts)

    # create instance of RemitTable1 and populate it with loaded data
    report = RemitTable1()
    report.add_data(remit_data)

    # save whole report to disk
    report.save(file_name=xml_filename)

    # validate xml code against xsd schema
    if schema_filename != "":
        ret_val = validate(xml_file=xml_filename, schema_file=schema_filename)

    # finish info
    script_finished = datetime.datetime.now()
    logger.setLevel(logging.INFO)  # always log
    logger.info("%s : ALL DONE in %s", script_finished, script_finished - script_started)
